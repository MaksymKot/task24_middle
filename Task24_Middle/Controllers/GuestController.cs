﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Models;

namespace Task24_Middle.Controllers
{
    public class GuestController : Controller
    {
        /// <summary></summary>
        /// <returns>A view of page with reviews</returns>
        [HttpGet]
        public ActionResult GuestPage()
        {
            var uow = new UnitOfWork();
            return View(uow.RevRepo.GetAll());
        }

        /// <summary>
        /// Add new review to database if data is valid
        /// </summary>
        /// <param name="review"></param>
        /// <returns>A view of page with reviews</returns>
        [HttpPost]
        public ActionResult AddReview(Review review)
        {
            if (string.IsNullOrEmpty(review.Text))
            {
                ModelState.AddModelError("Text", "You did not enter your review");
            }
            
            if (string.IsNullOrEmpty(review.AuthorName))
            {
                ModelState.AddModelError("AuthorName", "You have to enter your name");
            }
            var uow = new UnitOfWork();
            if (ModelState.IsValid)
                uow.RevRepo.Add(review);
            return View("GuestPage", uow.RevRepo.GetAll());
        }
    }
}