﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle;
using Task24_Middle.Models;

namespace Task24_Middle.Controllers
{
    public class FormController : Controller
    {
        /// <summary></summary>
        /// <returns>Page with survey</returns>
        [HttpGet]
        public ActionResult Form()
        {
            return View("FormPage");
        }
        /// <summary>
        /// Add new survey to database if data is valid
        /// </summary>
        /// <param name="data"></param>
        /// <returns>Redirect to GET method "FormPage" </returns>
        [HttpPost]
        public ActionResult FormPage(FormData data)
        {
            if (String.IsNullOrEmpty(data.LevelOfService))
                ModelState.AddModelError("LevelOfService", 
                    "Please choise a level of service");

            if (String.IsNullOrEmpty(data.PrimaryUsing))
                ModelState.AddModelError("PrimaryUsing", 
                    "Please tell about your primary using of this library");

            if (String.IsNullOrEmpty(data.Satisfaction))
                ModelState.AddModelError("Satisfaction", 
                    "Please choise your satisfation level");

            if (String.IsNullOrEmpty(data.Visit))
                ModelState.AddModelError("Visit", 
                    "Please say how often do you visit library");

            var uof = new UnitOfWork();

            if (ModelState.IsValid)
            {
                uof.FormRepo.Add(data);
                return RedirectToAction("FormPage");
            }
            else
                return View("FormPage");

        }
        
        /// <summary></summary>
        /// <returns>View of page with results of surveys</returns>
        [HttpGet]
        public ActionResult FormPage()
        {
            var uof = new UnitOfWork();
            return View("FormPageResult", uof.FormRepo.GetAll());
        }
    }
}