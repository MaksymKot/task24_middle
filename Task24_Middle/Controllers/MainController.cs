﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Task24_Middle.Models;

namespace Task24_Middle.Controllers
{
    public class MainController : Controller
    {
        /// <summary></summary>
        /// <returns>A view of main page</returns>
        public ActionResult MainPage()
        {
            var uow = new UnitOfWork();
            return View(uow.ArticlRepo.GetAll());
        }
    }
}