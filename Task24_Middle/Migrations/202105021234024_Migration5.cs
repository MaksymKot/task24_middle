﻿namespace Task24_Middle.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migration5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FormDatas", "satisfaction", c => c.String());
            DropColumn("dbo.FormDatas", "Satisfation");
        }
        
        public override void Down()
        {
            AddColumn("dbo.FormDatas", "Satisfation", c => c.String());
            DropColumn("dbo.FormDatas", "satisfaction");
        }
    }
}
