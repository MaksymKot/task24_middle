﻿namespace Task24_Middle.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migration4 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reviews", "Text", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reviews", "Text", c => c.String(nullable: false));
        }
    }
}
