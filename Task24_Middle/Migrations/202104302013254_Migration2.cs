﻿namespace Task24_Middle.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migration2 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Reviews", "AuthorName", c => c.String(maxLength: 20));
            AlterColumn("dbo.Reviews", "Text", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Reviews", "Text", c => c.String(nullable: false));
            AlterColumn("dbo.Reviews", "AuthorName", c => c.String(nullable: false, maxLength: 20));
        }
    }
}
