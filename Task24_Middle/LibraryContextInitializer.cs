﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Task24_Middle.Models;

namespace Task24_Middle
{
    public class LibraryContextInitializer : CreateDatabaseIfNotExists<LibraryDbContext>
    {
        protected override void Seed(LibraryDbContext context)
        {
            base.Seed(context);

            var article1 = new Article()
            {
                Date = DateTime.Now,
                Name = "Ivanhoe",
                Text = @"Ivanhoe: A Romance by Walter Scott is a historical novel 
                published in three volumes, in 1819, as one of the Waverley 
                novels. At the time it was written, the novel represented a 
                shift by Scott away from writing novels set in Scotland in the 
                fairly recent past to a more fanciful depiction of England in 
                the Middle Ages. Ivanhoe proved to be one of the best-known and 
                most influential of Scott's novels. "
            };

            var article2 = new Article()
            {
                Date = DateTime.Now,
                Name = "Macbeth",
                Text = @"Macbeth is a tragedy by William Shakespeare; it is thought to
                have been first performed in 1606. It dramatises the damaging 
                physical and psychological effects of political ambition on 
                those who seek power for its own sake."
            };

            var article3 = new Article()
            {
                Date = DateTime.Now,
                Name = "A Song of Ice and Fire",
                Text = @"A Song of Ice and Fire is a series of epic fantasy
                novels by the American novelist and screenwriter George R. 
                R. Martin. He began the first volume of the series, A Game 
                of Thrones, in 1991, and it was published in 1996"
            };

            context.Articles.Add(article1);
            context.Articles.Add(article2);
            context.Articles.Add(article3);

            var review1 = new Review()
            {
                AuthorName = "Jack",
                Date = DateTime.Now,
                Text = "I give \"Crime and Punishment\" 5 stars"
            };

            var review2 = new Review()
            {
                AuthorName = "John",
                Date = DateTime.Now,
                Text = "\"A song of Ice and Fire\" is amazing"
            };

            var review3 = new Review()
            {
                AuthorName = "Bob",
                Date = DateTime.Now,
                Text = "An extremely nice story about Harry Potter",
            };

            context.Reviews.Add(review1);
            context.Reviews.Add(review2);
            context.Reviews.Add(review3);

            context.SaveChanges();
        }
    }
}