﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Middle.Repositories;
using Task24_Middle.Models;

namespace Task24_Middle
{
    public class UnitOfWork
    {
        private LibraryDbContext db = new LibraryDbContext();
        private FormDataRepository formRepo;
        private ArticleRepository articlRepo;
        private ReviewRepository revRepo;

        /// <summary>
        /// Get an instance of form repository
        /// </summary>
        public FormDataRepository FormRepo 
        { 
            get 
            {
                if (formRepo is null)
                    formRepo = new FormDataRepository(db);
                return formRepo;
            } 
        }

        /// <summary>
        /// Get an instance of article repository
        /// </summary>
        public ArticleRepository ArticlRepo
        {
            get
            {
                if (articlRepo is null)
                    articlRepo = new ArticleRepository(db);
                return articlRepo;
            }
        }

        /// <summary>
        /// Get an instance of review repository
        /// </summary>
        public ReviewRepository RevRepo
        {
            get
            {
                if (revRepo is null)
                    revRepo = new ReviewRepository(db);
                return revRepo;
            }
        }
    }
}