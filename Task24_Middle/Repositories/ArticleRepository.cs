﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Middle.Models;


namespace Task24_Middle.Repositories
{
    public class ArticleRepository
    {
        /// <summary>
        /// An instance of database context
        /// </summary>
        private readonly LibraryDbContext db;

        public ArticleRepository(LibraryDbContext dbContext)
        {
            db = dbContext;
        }

        /// <summary>
        /// Add new article to database
        /// </summary>
        /// <param name="data"></param>
        public void Add(Article data)
        {
            db.Articles.Add(data);
            db.SaveChanges();
        }

        /// <summary>
        /// Get all articles from database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Article> GetAll()
        {
            return db.Articles;
        }
    }
}