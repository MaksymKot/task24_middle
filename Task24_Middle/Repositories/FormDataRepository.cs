﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Middle.Models;

namespace Task24_Middle.Repositories
{
    public class FormDataRepository
    {
        /// <summary>
        /// An private instance of database context
        /// </summary>
        private LibraryDbContext db;

        public FormDataRepository(LibraryDbContext dbContext)
        {
            db = dbContext;
        }

        /// <summary>
        /// Add new survey result to database
        /// </summary>
        /// <param name="data"></param>
        public void Add(FormData data)
        {
            db.FormsData.Add(data);
            db.SaveChanges();
        }

        /// <summary>
        /// Get all survey results from database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<FormData> GetAll()
        {
            return db.FormsData;
        }
    }
}