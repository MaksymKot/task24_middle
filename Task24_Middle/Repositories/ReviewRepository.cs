﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Task24_Middle.Models;

namespace Task24_Middle.Repositories
{
    public class ReviewRepository
    {
        /// <summary>
        /// An instance of database context
        /// </summary>
        private LibraryDbContext db;

        public ReviewRepository(LibraryDbContext dbContext)
        {
            db = dbContext;
        }

        /// <summary>
        /// Add new review to database
        /// </summary>
        /// <param name="data"></param>
        public void Add(Review data)
        {
            db.Reviews.Add(data);
            db.SaveChanges();
        }

        /// <summary>
        /// Get all reviews from database
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Review> GetAll()
        {
            return db.Reviews;
        }
    }
}