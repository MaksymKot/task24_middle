﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class Review
    {
        public int Id { get; set; }
        [StringLength(20, MinimumLength = 2, ErrorMessage = "Name's length must be from 2 to 20 letters")]
        public string AuthorName { get; set; }
        [DataType(DataType.Date)]
        public DateTime Date { get; set; }
        public string Text { get; set; }
    }
}