﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace Task24_Middle.Models
{
    public class LibraryDbContext : DbContext
    {
        public DbSet<Article> Articles { get; set; }
        public DbSet<Review> Reviews { get; set; }
        public DbSet<FormData> FormsData { get; set; }

        public LibraryDbContext() : base("Library")
        {
            Database.SetInitializer<LibraryDbContext>(
                new CreateDatabaseIfNotExists<LibraryDbContext>());
        }

    }
}