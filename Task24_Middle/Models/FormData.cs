﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Task24_Middle.Models
{
    public class FormData
    {
        public int Id { get; set; }
        public string Visit { get; set; }
        public string Satisfaction { get; set; }
        public string PrimaryUsing { get; set; }
        public string LevelOfService { get; set; }
    }
}